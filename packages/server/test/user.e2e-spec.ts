import { INestApplication } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import request from 'supertest'
import { Connection, createQueryBuilder } from 'typeorm'
import { AppModule } from '../src/app.module'
import { User } from '../src/user/user.entity'

describe('User API (e2e)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule]
    }).compile()

    app = moduleFixture.createNestApplication()
    await app.init()

    // Setup database
    const qry = createQueryBuilder()
    await qry
      .delete()
      .from(User)
      .execute()
  })

  afterAll(async () => {
    // Cleanup database
    const qry = createQueryBuilder()
    await qry
      .delete()
      .from(User)
      .execute()
    const connection = app.get(Connection)
    await connection.close()
  })

  describe('/auth/local', () => {
    const sampleuser = {
      username: 'testuser',
      password: 'testpassword',
      email: 'email@network.com',
      fullName: 'Test'
    }
    let newuser: any
    it('shuold be able to register and return user without password', done => {
      return request(app.getHttpServer())
        .post('/auth/local/register')
        .send(sampleuser)
        .expect(201)
        .end((err, res) => {
          if (err) {
            done(err)
          }
          newuser = res.body
          expect(newuser.password).toBeUndefined()
          done()
        })
    })
    it('shuold be able to login with same user and get token', done => {
      return request(app.getHttpServer())
        .post('/auth/local/')
        .send({
          username: sampleuser.username,
          password: sampleuser.password
        })
        .expect(201)
        .end((err, res) => {
          if (err) {
            done(err)
          }
          const logindata = res.body
          expect(logindata.user).toBeDefined()
          expect(logindata.token).toBeDefined()
          expect(logindata.user.password).toBeUndefined()
          done()
        })
    })
  })
})
