import { INestApplication } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import request from 'supertest'
import { Connection, createQueryBuilder } from 'typeorm'
import { AppModule } from '../src/app.module'
import { User } from '../src/user/user.entity'
import { Employee } from '../src/employee/employee.entity'

describe('AppController (e2e)', () => {
  let app: INestApplication
  let token: string

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule]
    }).compile()

    app = moduleFixture.createNestApplication()
    await app.init()

    // Setup database
    const qry = createQueryBuilder()
    await qry
      .delete()
      .from(Employee)
      .execute()
    await qry
      .delete()
      .from(User)
      .execute()
    const u1 = new User()
    u1.fullName = 'First Second'
    u1.email = 'first.second@domain.com'
    u1.username = 'username'
    u1.password = '$2b$10$IqwMpSua6GLwH8KXWf71UeUR7GlfDyMp0gJxljKOYmW4jGksmwDVG'
    await u1.save()
    // Login
    const resp = await request(app.getHttpServer())
      .post('/auth/local/')
      .send({
        username: 'username',
        password: 'passwsss'
      })
      .catch(err => {
        console.error(err)
        throw err
      })
    expect(resp.status).toEqual(201)
    expect(resp.body).toBeDefined()
    expect(resp.body.token).toBeDefined()
    token = 'Bearer ' + resp.body.token
  })

  afterAll(async () => {
    // Cleanup database
    const qry = createQueryBuilder()
    await qry
      .delete()
      .from(Employee)
      .execute()
    await qry
      .delete()
      .from(User)
      .execute()
    const connection = app.get(Connection)
    await connection.close()
  })

  it('/employee (GET) - forbids list of employees if unauthrized', async () => {
    const response = await request(app.getHttpServer())
      .get('/employee')
      .expect(401)
  })

  it('/employee (GET) - returns list of employees', async () => {
    const response = await request(app.getHttpServer())
      .get('/employee')
      .set('Authorization', token)
      .expect(200)
      .expect([])
  })

  it('/employee (POST) - able to create employee', async () => {
    const response = await request(app.getHttpServer())
      .post('/employee')
      .send({
        firstname: 'Test2',
        lastname: 'Lastname',
        description: 'employee description'
      })
      .set('Authorization', token)
      .expect(201)
    expect(response.body).toBeDefined()
    expect(response.body.id).toBeDefined()
    const responselist = await request(app.getHttpServer())
      .get('/employee')
      .set('Authorization', token)
      .expect(200)
    expect(responselist.body).toBeDefined()
    expect(responselist.body).toEqual(
      expect.arrayContaining([
        {
          id: response.body.id,
          firstname: 'Test2',
          lastname: 'Lastname',
          description: 'employee description'
        }
      ])
    )
  })
})
