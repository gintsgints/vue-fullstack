import { INestApplication } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import request from 'supertest'
import { Connection, createQueryBuilder } from 'typeorm'
import { AppModule } from '../src/app.module'
import { User } from '../src/user/user.entity'

describe('AppController (e2e)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule]
    }).compile()

    app = moduleFixture.createNestApplication()
    await app.init()

    // Setup database
    const qry = createQueryBuilder()
    await qry
      .delete()
      .from(User)
      .execute()
  })

  afterAll(async () => {
    // Cleanup database
    const qry = createQueryBuilder()
    await qry
      .delete()
      .from(User)
      .execute()
    const connection = app.get(Connection)
    await connection.close()
  })

  it('/ (GET)', async () => {
    await request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!')
  })
})
