import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { Employee } from './employee.entity'
import { EmployeeDTO } from './dto/employee.dto'
import { BaseService } from '../shared/base.service'

@Injectable()
export class EmployeeService extends BaseService<Employee, EmployeeDTO> {
  constructor(
    @InjectRepository(Employee)
    private readonly empRepository: Repository<Employee>
  ) {
    super()
    this.repository = empRepository
  }
}
