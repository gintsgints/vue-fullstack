import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  UsePipes,
  Param,
  Body,
  UseGuards,
  Query
} from '@nestjs/common'
import { ApiBearerAuth } from '@nestjs/swagger'
import { AuthGuard } from '@nestjs/passport'

import { EmployeeService } from './employee.service'
import { Employee } from './employee.entity'
import { EmployeeDTO } from './dto/employee.dto'
import { ValidationPipe } from '../shared/Validation.pipe'
import { Roles } from '../shared/decorators/roles.decorator'
import { UserRole } from '../shared/UserRole'
import { RolesGuard } from '../shared/guards/roles.guard'

@Controller('/employee')
@ApiBearerAuth()
export class EmployeeController {
  constructor(private readonly employeeService: EmployeeService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async index(@Query() query?): Promise<Employee[]> {
    return await this.employeeService.index(query)
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  async findOne(@Param('id') id: string): Promise<Employee> {
    return await this.employeeService.show(id)
  }

  @Post()
  @UsePipes(new ValidationPipe())
  @Roles(UserRole.user, UserRole.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async create(@Body() data: EmployeeDTO): Promise<Employee> {
    return await this.employeeService.create(data)
  }

  @Put(':id')
  @UsePipes(new ValidationPipe())
  @Roles(UserRole.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async update(
    @Param('id') id: string,
    @Body() data: EmployeeDTO
  ): Promise<Employee> {
    return await this.employeeService.update(id, data)
  }

  @Delete(':id')
  @Roles(UserRole.admin)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async delete(@Param('id') id: string) {
    return await this.employeeService.destroy(id)
  }
}
