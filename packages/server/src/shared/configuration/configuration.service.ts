import { Injectable } from '@nestjs/common'
import { Configuration } from './configuration.enum'

require('dotenv').config()

@Injectable()
export class ConfigurationService {
  static readonly dbConfig: any = {
    type:
      process.env.TYPEORM_CONNECTION || process.env.DB_CONNECTION || 'postgres',
    host: process.env.TYPEORM_HOST || process.env.DB_HOST || 'localhost',
    port: process.env.TYPEORM_PORT || process.env.DB_PORT || 5432,
    username:
      process.env.TYPEORM_USERNAME || process.env.DB_USERNAME || 'postgres',
    password:
      process.env.TYPEORM_PASSWORD || process.env.DB_PASSWORD || 'postgres_234',
    database:
      process.env.TYPEORM_DATABASE || process.env.DB_DATABASE || 'postgres',
    entities: [
      process.env.TYPEORM_ENTITIES ||
        process.env.DB_ENTITIES ||
        'src/**/**.entity{.ts,.js}'
    ],
    synchronize:
      process.env.TYPEORM_SYNCHRONIZE === 'true' ||
      process.env.DB_SYNCHRONIZE === 'true' ||
      false,
    logging:
      process.env.TYPEORM_LOGGING === 'true' ||
      process.env.DB_LOGGING === 'true' ||
      false
  }
  private hosting: string = process.env.NODE_ENV || 'develop'

  get(name: string): string {
    return process.env[name]
  }

  get isDevelopment(): boolean {
    return this.hosting === 'develop'
  }
}
