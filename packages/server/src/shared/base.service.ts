import { Repository, Like, Between, FindManyOptions, BaseEntity } from 'typeorm'
import { HttpStatus, HttpException } from '@nestjs/common'

export interface GetOptions {
  skip?: number
  take?: number
  where: string
}

export class BaseService<T extends BaseEntity, TDTO> {
  protected repository: Repository<T>

  async index(query?): Promise<T[]> {
    return await this.repository.find(this.findOptions(query))
  }

  async show(id: string): Promise<T> {
    const data = await this.repository.findOne(id)
    if (!data) {
      throw new HttpException('Not Found', HttpStatus.NOT_FOUND)
    }
    return data
  }

  async create(data: TDTO): Promise<T> {
    const newdata = await this.repository.create(data)
    return await newdata.save()
  }

  async update(id: string, data: Partial<TDTO>): Promise<T> {
    const upddata = await this.repository.findOne(id)
    if (!upddata) {
      throw new HttpException('Not Found', HttpStatus.NOT_FOUND)
    }
    Object.assign(upddata, data)
    return await upddata.save()
  }

  async destroy(id: string) {
    await this.repository.delete(id)
    return { deleted: true }
  }

  // helper functions
  private findOptions(query?): FindManyOptions {
    if (!query) {
      return {}
    }

    const options: FindManyOptions = {
      skip: 0,
      take: 100
    }

    if (query.skip) {
      options.skip = query.skip
    }

    if (query.take) {
      options.take = query.take
    }

    if (query.where) {
      const wherereq = JSON.parse(query.where)
      const whereres: Object = {}

      for (const key in wherereq) {
        if (
          typeof wherereq[key] === 'string' &&
          wherereq[key].charAt(wherereq[key].length - 1) === '%'
        ) {
          whereres[key] = Like(wherereq[key])
        } else {
          // You can describe where properties in object
          if (typeof wherereq[key] === 'object') {
            // If type is date
            if (wherereq[key].type && wherereq[key].type === 'date') {
              // you have to specify where datefrom & dateto values
              whereres[key] = Between(
                wherereq[key].datefrom,
                wherereq[key].dateto
              )
            } else {
              whereres[key] = wherereq[key]
            }
          } else {
            whereres[key] = wherereq[key]
          }
        }
      }

      options.where = whereres
    }
    return options
  }
}
