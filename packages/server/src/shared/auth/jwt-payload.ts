import { UserRole } from '../UserRole'

export interface JwtPayload {
  username: string
  role: UserRole
  iat?: Date
}
