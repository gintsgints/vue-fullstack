import { UserRole } from '../../shared/UserRole'
import { ReflectMetadata } from '@nestjs/common'

export const Roles = (...roles: UserRole[]) => ReflectMetadata('roles', roles)
