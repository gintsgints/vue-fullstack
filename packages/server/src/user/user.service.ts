import {
  Injectable,
  HttpException,
  HttpStatus,
  forwardRef,
  Inject
} from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { User } from './user.entity'
import { Repository } from 'typeorm'
import { UserLoginDTO } from './dto/UserLogin.dto'
import { UserRegisterDTO } from './dto/UserRegister.dto'
import { genSalt, hash, compare } from 'bcryptjs'
import { plainToClass } from 'class-transformer'
import { UserDTO } from './dto/User.dto'
import { LoginResponseDTO } from './dto/LoginResponse.dto'
import { AuthService } from '../shared/auth/auth.service'

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @Inject(forwardRef(() => AuthService))
    readonly authService: AuthService
  ) {}

  async register(data: UserRegisterDTO): Promise<UserDTO> {
    try {
      const newUser = await this.userRepository.create(data)
      const salt = await genSalt(10)
      newUser.password = await hash(newUser.password, salt)
      const saveduser = await newUser.save()
      return plainToClass(UserDTO, saveduser)
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  async login(data: UserLoginDTO): Promise<LoginResponseDTO> {
    const { username, password } = data

    const user = await this.userRepository.findOne({ username })
    if (!user) {
      throw new HttpException('Invalid credentials', HttpStatus.BAD_REQUEST)
    }

    const isMatch = await compare(password, user.password)
    if (!isMatch) {
      throw new HttpException('Invalid credentials', HttpStatus.BAD_REQUEST)
    }

    const plainuser = plainToClass(UserDTO, user)

    const payload = {
      username: user.username,
      role: user.role
    }

    const token = await this.authService.signPayload(payload)
    return { token, user: plainuser }
  }

  async findOne(filter: {}): Promise<User> {
    return await this.userRepository.findOne(filter)
  }
}
