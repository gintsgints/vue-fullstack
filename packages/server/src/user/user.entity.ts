import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  Column,
  BaseEntity
} from 'typeorm'

import { UserRole } from '../shared/UserRole'

@Entity('user')
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @CreateDateColumn()
  created: Date

  @Column('text')
  fullName: string

  @Column({ default: 'User' })
  role?: UserRole

  @Column({
    type: 'text',
    unique: true
  })
  email: string

  @Column({
    type: 'text',
    unique: true
  })
  username: string

  @Column('text')
  password: string
}
