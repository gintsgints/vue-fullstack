import { Injectable, Inject, HttpStatus, HttpException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { Photo } from './photo.entity'
import { PhotoDTO } from './dto/photo.dto'
import { BaseService } from '../shared/base.service'

@Injectable()
export class PhotoService extends BaseService<Photo, PhotoDTO> {
  constructor(
    @InjectRepository(Photo)
    private readonly photoRepository: Repository<Photo>
  ) {
    super()
    this.repository = photoRepository
  }
}
