import { Test, TestingModule } from '@nestjs/testing'
import { getRepositoryToken } from '@nestjs/typeorm'
import { PhotoController } from './photo.controller'
import { PhotoService } from './photo.service'

describe('Photo Controller', () => {
  let module: TestingModule
  let controller: PhotoController

  const mockService = {
    index() {
      return []
    }
  }

  beforeAll(async () => {})

  it('should be defined', async () => {
    module = await Test.createTestingModule({
      controllers: [PhotoController],
      providers: [
        {
          provide: PhotoService,
          useValue: mockService
        }
      ]
    }).compile()
    controller = module.get<PhotoController>(PhotoController)
    expect(controller).toBeDefined()
  })
})
