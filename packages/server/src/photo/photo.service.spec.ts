import { Test, TestingModule } from '@nestjs/testing'
import { getRepositoryToken } from '@nestjs/typeorm'
import { PhotoService } from './photo.service'
import { Photo } from './photo.entity'

describe('PhotoService', () => {
  let service: PhotoService
  const mockRepository = {
    find() {
      return [
        {
          id: 1,
          name: 'Photo name',
          description: 'Photo description'
        },
        {
          id: 2,
          name: 'Photo 2 name',
          description: 'Photo 2 description'
        }
      ]
    }
  }

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PhotoService,
        {
          provide: getRepositoryToken(Photo),
          useValue: mockRepository
        }
      ]
    }).compile()
    service = module.get<PhotoService>(PhotoService)
  })
  it('should be defined', () => {
    expect(service).toBeDefined()
  })
  it('should return list of photos for index', async () => {
    const photos = await service.index()
    expect(photos).toEqual([
      { description: 'Photo description', id: 1, name: 'Photo name' },
      { description: 'Photo 2 description', id: 2, name: 'Photo 2 name' }
    ])
  })
})
