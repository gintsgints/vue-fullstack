import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { seed } from './seed'
import { Logger } from '@nestjs/common'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
const helmet = require('helmet')
const compression = require('compression')
import pck from '../package.json'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  app.use(helmet())
  app.use(compression())

  if (AppModule.isDev) {
    seed()
  }

  const hostDomain = AppModule.isDev
    ? `${AppModule.host}:${AppModule.port}`
    : AppModule.host
  const swaggerOptions = new DocumentBuilder()
    .setTitle('VUE Backend')
    .setDescription('API Documentation')
    .setVersion(pck.version)
    .setHost(hostDomain.split('//')[1])
    .setSchemes(AppModule.isDev ? 'http' : 'https')
    .setBasePath('/api')
    .addBearerAuth('Authorization', 'header')
    .build()

  const swaggerDoc = SwaggerModule.createDocument(app, swaggerOptions)

  SwaggerModule.setup('/api/docs', app, swaggerDoc, {
    swaggerUrl: `${hostDomain}/api/docs-json`,
    explorer: true,
    swaggerOptions: {
      docExpansion: 'list',
      filter: true,
      showRequestDuration: true
    }
  })

  app.setGlobalPrefix('api')

  await app.listen(AppModule.port)

  Logger.log(
    `Server running at http://${AppModule.host}:${AppModule.port} `,
    'bootstrap'
  )
}
bootstrap()
