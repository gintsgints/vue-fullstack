import axios from 'axios'
import users from '../store/modules/users.module'

export default function setup() {
  axios.interceptors.request.use(
    (config) => {
      if (users.token) {
        config.headers.Authorization = `Bearer ${users.token}`
      }
      return config
    },
    (err) => {
      return Promise.reject(err)
    }
  )
}
