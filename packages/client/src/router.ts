import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Code403 from './views/Code403.vue'
import users from './store/modules/users.module'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/photo/',
      name: 'photo',
      component: () => import('@/views/Photo.vue'),
      meta: {
        requiresAuth: true,
        roles: ['User', 'Admin']
      }
    },
    {
      path: '/employee/',
      name: 'employee',
      component: () => import('@/views/Employee.vue'),
      meta: {
        requiresAuth: true,
        roles: ['User', 'Admin']
      }
    },
    {
      path: '/about/',
      name: 'about',
      component: () => import('@/views/About.vue')
    },
    {
      path: '/login/',
      name: 'login',
      component: () => import('@/views/Login.vue')
    },
    {
      path: '/403/',
      name: '403',
      component: () => import('@/views/Code403.vue')
    },
    {
      path: '/register/',
      name: 'register',
      component: () => import('@/views/Register.vue')
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (users.user === null) {
      const data = localStorage.getItem('bda-user')
      if (data) {
        users.setUser(JSON.parse(data))
      }
    }
    if (users.user === null) {
      next({
        path: '/login',
        params: { nextUrl: to.fullPath }
      })
    } else {
      const userdata = users.user
      if (
        to.matched.some((record) => {
          return record.meta.roles.some((role: any) => {
            return role === userdata.user.role
          })
        })
      ) {
        next()
      } else {
        next({ name: '403' })
      }
    }
  } else {
    next()
  }
})

export default router
