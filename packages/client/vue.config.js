module.exports = {
  pluginOptions: {
	i18n: {
      enableInSFC: true
    }
  },

  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:3001',
        ws: true
        // changeOrigin: true
      }
    }
  }
}
