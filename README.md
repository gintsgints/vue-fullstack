# VUE fullstack application

[![pipeline status](https://gitlab.com/gintsgints/vue-fullstack/badges/master/pipeline.svg)](https://gitlab.com/gintsgints/vue-fullstack/commits/master)

[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

You can check [working project here](http://vue.hominum.eu).
You can do full deploy yourself, on kubernetes cluster using [helm charts](./helm)
Or local or remote docker, using [provided docker compose file](https://gitlab.com/gintsgints/vue-fullstack/blob/master/docker-compose.yml)

# Components used

Project consists of frontend & backend packages, made as [Yarn workspaces](https://yarnpkg.com/lang/en/docs/workspaces/).
Packages are made mainly using [TypeScript](https://www.typescriptlang.org/) precompiler.
Project also contains all type of tests. Starting from unit tests and finishing by End2End tests.
All this is hosted and automaticaly [built](https://gitlab.com/gintsgints/vue-fullstack/pipelines) in to [docker](https://www.docker.com/) [images](https://gitlab.com/gintsgints/vue-fullstack/container_registry) thaks to [GitLab](https://gitlab.com/)

## Main frontend features

- [VUE](https://vuejs.org/) as a main UI construction tool
- [VUETIFY](https://vuetifyjs.com/en/) for components
- Little VUE additions like [i18n](https://github.com/kazupon/vue-i18n) & [axios](https://github.com/yugasun/vue-axios-plugin)

## Main backend features

- REST API made using [NESTJS framework](https://nestjs.com/)
- Authorization using [JavaScript Web Token](https://jwt.io/)
- [Swagger](https://swagger.io/) for endpoint documentation
- Validation

# Development

Prepeare your environiment before development. I would recomend using Visual Studio Code with plugins:

- DotENV - for environiment files
- GitLens - for better GIT expierience
- Prettier - for code auto format
- StandardJS - for javascript [standard](https://standardjs.com/) support
- TSLint - For typescript error highlight
- Vetur - For vue support (Shortcuts, syntax e.t.c.)
- YAML - for Yaml file format support

```bash
#Copy environment settings example to both client & server and edit according to you
cp ./packages/client/.example.env ./packages/client/.env
cp ./packages/server/.example.env ./packages/server/.env

#Start database
docker-compose up -d db
#And start migrations, to crate database objects


#In one console start backend
cd ./packages/server/
yarn start:dev

#In other console start frontend
cd ./packages/client/
yarn start:dev
```
